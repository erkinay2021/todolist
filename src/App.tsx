import React from "react";
import { Switch, Route } from "react-router-dom";
import { Navbar } from "./Components/navbar";
import { Todo } from "./Components/Todo/Todo";
import { About } from "./Components/about";

const App: React.FC = () => {
  return (
    <>
      <Navbar />
      <Switch>
        <Route component={Todo} path="/" exact />
        <Route component={About} path="/about" />
      </Switch>
    </>
  );
};

export default App;
