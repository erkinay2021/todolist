import React from "react";
import { ITodo } from "../../interfaces";

type TodoListProps = {
  todos: ITodo[];
  onToggle(id: number): void;
  onRemove(id: number): void;
};

export const TodoList: React.FC<TodoListProps> = (props) => {
  const removeHandler = (event: React.MouseEvent, id: number) => {
    event.preventDefault();
    props.onRemove(id);
  };
  if (props.todos.length === 0) {
    return <p className="center">You have no tasks yet</p>;
  }
  return (
    <ul>
      {props.todos.map((todo) => {
        const classes = ["todo"];
        if (todo.completed) {
          classes.push("completed");
        }
        return (
          <li key={todo.id} className={classes.join(" ")}>
            <label>
              <input
                type="checkbox"
                checked={todo.completed}
                onChange={() => props.onToggle(todo.id)}
              />
              <span>{todo.title}</span>
              <i
                className="material-icons red-text"
                onClick={(e) => removeHandler(e, todo.id)}
              >
                delete
              </i>
            </label>
          </li>
        );
      })}
    </ul>
  );
};
