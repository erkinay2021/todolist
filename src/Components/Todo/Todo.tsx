import React, { useState, useEffect } from "react";
import { TodoForm } from "./TodoForm";
import { TodoList } from "./List";
import { ITodo } from "../../interfaces";

export const Todo: React.FC = () => {
  const [todos, setTodos] = useState<ITodo[]>([]);
  useEffect(() => {
    const savedTodos = JSON.parse(localStorage.getItem("todos") || "[]");
    setTodos(savedTodos);
  }, []);
  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);
  const addTodos = (title: string) => {
    if (title.trim()) {
      const newTodo: ITodo = {
        title,
        id: Date.now(),
        completed: false,
      };
      setTodos((prev) => [newTodo, ...prev]);
    }
  };
  const onToggleHandler = (id: number) => {
    setTodos((prev) =>
      prev.map((todo) => {
        if (todo.id === id) {
          return { ...todo, completed: !todo.completed };
        }
        return todo;
      })
    );
  };
  const onRemoveHandler = (id: number) => {
    const confirmHandler: boolean = window.confirm(
      "You definitely want to delete this task?"
    );
    if (confirmHandler) {
      setTodos((prev) => prev.filter((todo) => id !== todo.id));
    }
  };
  return (
    <div className="container">
      <TodoForm onAdd={addTodos} todoList={todos} />
      <TodoList
        todos={todos}
        onToggle={onToggleHandler}
        onRemove={onRemoveHandler}
      />
    </div>
  );
};
