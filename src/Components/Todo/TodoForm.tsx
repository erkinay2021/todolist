import React, { useState } from "react";
import { ITodo } from "../../interfaces";

interface ITodoFormProps {
  onAdd(title: string): void;
  todoList: ITodo[];
}

export const TodoForm: React.FC<ITodoFormProps> = (props) => {
  const [title, setTitle] = useState<string>("");
  const changeTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value);
  };
  const onEnterPress = (event: React.KeyboardEvent) => {
    if (event.key === "Enter") {
      let checkTitle: boolean = props.todoList.some(
        (todo) => todo.title === title
      );
      if (checkTitle) {
        alert("You have a task with that name. Have you forgotten about her?");
      } else {
        props.onAdd(title);
        setTitle("");
      }
    }
  };
  return (
    <div
      className="input-field"
      style={{
        marginTop: "2rem",
      }}
    >
      <label htmlFor="title" className="active">
        Enter task name
      </label>
      <input
        id="title"
        type="text"
        placeholder="Enter task name"
        value={title}
        onChange={changeTitle}
        onKeyPress={onEnterPress}
      />
    </div>
  );
};
