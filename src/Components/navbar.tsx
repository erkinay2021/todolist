import React from "react";
import { NavLink } from "react-router-dom";

export const Navbar: React.FC = () => {
  return (
    <nav>
      <div className="nav-wrapper indigo darken-3" style={{padding: '0 20px'}}>
        <NavLink to="/" className="brand-logo">
          Dairy Planner
        </NavLink>
        <ul className="right hide-on-med-and-down">
          <li>
            <NavLink to="/">Tasks</NavLink>
          </li>
          <li>
            <NavLink to="/about">Information</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};
